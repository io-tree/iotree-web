import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import { isAuthenticated } from "./services/auth";

import Home from './pages/Home/';
import Checker from './pages/Verification';
import RecoveryPassword from './pages/RecoveryPassword';
import Login from './pages/Login';
import ForgotPassword from './pages/ForgotPassword';
import Dashboard from './pages/Dashboard';

const PrivateRoute = ({ component: Component, ...rest }) => (
	<Route
		{...rest}
		render={props =>
			isAuthenticated() ? (
				<Component {...props} />
			) : (
				<Redirect to={{ pathname: "/login", state: { from: props.location } }} />
			)
		}
	/>
);

export default function Routes({history}) {
	return(
		<BrowserRouter>
			<Switch>
				<Route path='/' exact component={Home} history={history}/>
				
				<Route path='/verification/:hash' exact component={Checker} history={history}/>
				<Route path='/password/recovery/:hash' exact component={RecoveryPassword} history={history}/>
				<Route path='/password/forgot' exact component={ForgotPassword} history={history}/>

				<Route path='/login' component={Login} history={history}/>

				<PrivateRoute path="/dashboard" component={Dashboard} />

				<Route path='*' component={Home} />
			</Switch>
		</BrowserRouter>
	)
}