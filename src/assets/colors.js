export default {
	primaryLight: '#6AFFA6',
	primary: '#00E676',
	primaryDark: '#00B248',

	secondary: '#0077ff',
	secondaryLight: '#01BAEF',

	reallyWhite: '#FFFFFF',
	white: '#FAFAFA',

	grayLight: '#e3e3e3',
	gray: '#CECECE',
	grayDark:'#A7A0A0',
	
	black: '#707070',
	reallyBlack: '#000000'
}