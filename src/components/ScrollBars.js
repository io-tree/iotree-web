import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

export default CustomScrollbars = props => {
    return (
		<Scrollbars
			renderTrackHorizontal={props => <div {...props} className="track-horizontal"/>}
			renderTrackVertical={props => <div {...props} className="track-vertical"/>}
			renderThumbHorizontal={props => <div {...props} className="thumb-horizontal"/>}
			renderThumbVertical={props => <div {...props} className="thumb-vertical"/>}
			renderView={props => <div {...props} className="view"/>}
		>
			{props.children}
		</Scrollbars>
    );
}