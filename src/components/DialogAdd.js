import React, {useState, useEffect} from 'react';
import {
	Dialog,
	AppBar,
	Toolbar,
	Typography,
	IconButton,
	Button,
	CircularProgress,
	Slide
} from '@material-ui/core';
import {
	FaTimes
} from 'react-icons/fa';
import { makeStyles } from '@material-ui/core/styles';
import { Scrollbars } from 'react-custom-scrollbars';

import LoadingScreen from './LoadingScreen';

const useStyle = makeStyles( theme => ({
	appBar: {
		position: 'relative',
		color: 'white',
		height: 50
	},
	buttonSave: {
		position: 'absolute',
		right: 10,
		color: 'white',
		fontSize: 17
	}
}));

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />;
});


export default (props) => {

	const classes = useStyle();

	return (
		<Dialog fullScreen open={props.onOpen} onClose={() => props.onClose(false)} TransitionComponent={Transition}>
			<AppBar className={classes.appBar}>
				<Toolbar>
					<IconButton edge="start" color="inherit" onClick={() => props.onClose(false)} aria-label="close" disabled={props.loading}>
						<FaTimes />
					</IconButton>
					<Typography variant="h6">
						{props.label}
					</Typography>
					<Button
						onClick={props.onClickSave}
						variant="text"
						className={classes.buttonSave}
					>{
						props.loading ? <CircularProgress color={'inherit'}/> : "Salvar"
					}</Button>
				</Toolbar>
			</AppBar>
			<LoadingScreen enabled={props.loading} />
			<Scrollbars>
				{props.children}
			</Scrollbars>
		</Dialog>
	)
}