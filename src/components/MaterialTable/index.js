import React, { forwardRef, useState } from 'react'
import {
  Container
} from '@material-ui/core'
import {
  FaPlusCircle,
  FaCheck,
  FaTimes,
  FaTrash,
  FaAngleLeft,
  FaEdit,
  FaSave,
  FaFilter,
  FaAngleDoubleLeft,
  FaAngleDoubleRight,
  FaAngleRight,
  FaSearch,
  FaCaretDown,
  FaMinus,
  FaColumns
} from 'react-icons/fa'
import MaterialTable from "material-table";

import useStyles from './styles'

export default ({ title, data, columns, actions, detailPanel, components, isEditable, isEditHidden, isDeletable, isDeleteHidden, onRowAdd, onRowUpdate, onRowDelete, options }) => {

  const classes = useStyles()
  const tableIcons = {
    Add: forwardRef((props, ref) => <FaPlusCircle size={35} ref={ref} />),
    Check: forwardRef((props, ref) => <FaCheck color={"#0DC800"} ref={ref} />),
    Clear: forwardRef((props, ref) => <FaTimes color={"#DB0000"} ref={ref} />),
    Delete: forwardRef((props, ref) => <FaTrash color={"#DB0000"} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <FaAngleRight ref={ref} />),
    Edit: forwardRef((props, ref) => <FaEdit color={"#2E6C8D"} ref={ref} />),
    Export: forwardRef((props, ref) => <FaSave ref={ref} />),
    Filter: forwardRef((props, ref) => <FaFilter ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FaAngleDoubleLeft ref={ref} />),
    LastPage: forwardRef((props, ref) => <FaAngleDoubleRight ref={ref} />),
    NextPage: forwardRef((props, ref) => <FaAngleRight ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <FaAngleLeft ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <FaTimes ref={ref} />),
    Search: forwardRef((props, ref) => <FaSearch />),
    SortArrow: forwardRef((props, ref) => <FaCaretDown ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <FaMinus ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <FaColumns ref={ref} />)
  };

  return (
    <MaterialTable
      icons={tableIcons}
      columns={columns}
      data={data}
      title={title}
      actions={actions}
      detailPanel={detailPanel}
      editable={{
        isEditable,
        isEditHidden,
        isDeletable,
        isDeleteHidden,
        onRowAdd,
        onRowUpdate,
        onRowDelete,
      }}
      options={{
        actionsColumnIndex: -1,
        pageSize: 10,
        //padding: 'dense',
        pageSizeOptions: [],
        ...options
      }}
      components={{
        ...components,
        Container: props => (
          <Container
            {...props}
            disableGutters
            maxWidth={false}
            className={classes.mainContainer}
          />
        )
      }}
      localization={{
        pagination: {
          labelDisplayedRows: '{from}-{to} de {count}',
          labelRowsSelect: 'Linhas',
          labelRowsPerPage: 'Linhas por página',
          firstAriaLabel: 'Primeira página',
          firstTooltip: 'Primeira página',
          previousAriaLabel: 'Página anterior',
          previousTooltip: 'Página anterior',
          nextAriaLabel: 'Próxima página',
          nextTooltip: 'Próxima página',
          lastAriaLabel: 'Última página',
          lastTooltip: 'Última página'
        },
        toolbar: {
          nRowsSelected: '{0} linha(s) selecionada(s)',
          searchPlaceholder: 'Pesquisar',
          searchTooltip: 'Pesquisar'
        },
        header: {
          actions: 'Ações'
        },
        body: {
          emptyDataSourceMessage: 'Nenhum dado para mostrar',
          filterRow: {
            filterTooltip: 'Filtro'
          },
          addTooltip: 'Adicionar',
          deleteTooltip: 'Deletar',
          editTooltip: 'Editar',
          editRow: {
            deleteText: 'Tem certeza em deletar essa linha?',
            cancelTooltip: 'Cancelar',
            saveTooltip: 'Salvar'
          }
        }
      }}
    />
  )
}