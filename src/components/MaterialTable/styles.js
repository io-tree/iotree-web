import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  mainContainer: {
    width: 'calc(100% - 10px)',
    height: '80%',
    position: 'relative',
    backgroundColor: 'white',
    borderRadius: 10,
    border: '1px solid rgba(0,0,0,0.1)',
    boxShadow: '8px 8px 8px rgba(0,0,0,0.2)',
    padding: 15
  },
}))