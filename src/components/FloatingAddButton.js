import React from 'react';
import {
	Fab,
	Typography,
} from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { FaPlus } from 'react-icons/fa';

const useStyle = makeStyles( theme => ({
	addButton: {
		position: "fixed",
		bottom: 50,
		right: 60,
		color: 'white',
		"& p": {
			width: 0,
			padding: 0,
			overflow: 'hidden',
			transition: theme.transitions.create(['width'], {
				easing: theme.transitions.easing.sharp,
				duration: 280,
			}),
		},
		"&:hover p": {
			width: 150
		}
	}
}));

export default ({ onClick, label }) => {

	const classes = useStyle();

	return (
		<Fab 
			color="primary" 
			variant="extended" 
			className={classes.addButton} 
			onClick={onClick}
		>
			<FaPlus />
			<Typography variant={"overline"} component={'p'}>
				{label}
			</Typography>
		</Fab>
	)
}