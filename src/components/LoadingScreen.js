import React, { useState } from 'react';

import {
  Container,
  Typography,
} from '@material-ui/core';
import { Zoom, Flip, Fade } from 'react-reveal';
import Lottie from 'react-lottie';

import loading from '../assets/loading.json'

export default ({ enabled }) => {

  const [appear, setAppear] = useState(1);

  return (
    <Container maxWidth={false} style={{
      display: (enabled ? 'flex' : 'none'),
      position: 'absolute',
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      backgroundColor: 'rgba(0,0,0,0.6)',
      zIndex: 1000
    }}>
      <Fade duration={2000}>
        <Lottie
          options={{
            loop: true,
            autoplay: true,
            animationData: loading,
            rendererSettings: {
              preserveAspectRatio: 'xMidYMid slice'
            }
          }}
          speed={2}
          width={200}
          height={200}
        />
        <Flip right cascade duration={2000} forever>
          <Typography style={{ color: 'white' }}>
            Aguarde
						</Typography>
        </Flip>
      </Fade>
    </Container>
  )
}