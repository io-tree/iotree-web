import axios from 'axios';
import {
  encrypt,
  decrypt
} from './crypt';

import {
  getToken,
  setToken,
  deleteToken
} from './auth';


const api = axios.create({
  baseURL: process.env.REACT_APP_URL_API + '/api/v1',
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  }
});

//Antes de enviar
api.interceptors.request.use(async (request) => {
  console.log('request', request);
  if (process.env.REACT_APP_SECURITY_ENABLED === 'true') {
    request.data = { key: encrypt(JSON.stringify(request.data)) };
  }
  const { token, refreshToken } = getToken();

  request.headers.Authorization = 'Bearer ' + token;
  request.headers['refresh-token'] = refreshToken;

  const language = (navigator.language || navigator.userLanguage).toLowerCase();
  request.headers.language = language;

  return request;
});

//Antes de chegar
api.interceptors.response.use(async (response) => {
  console.log('response', response)
  if (process.env.REACT_APP_SECURITY_ENABLED === 'true') {
    response.data = JSON.parse(decrypt(response.data.key));
  }
  if (response.data.token) {
    setToken(response.data.token, response.data.refreshToken)
  }
  if (response.data.code === "userNotAuthorization") {
    console.log(window.location)
    deleteToken();
    alert(response.data.msg)
    window.location.href = window.location.origin + "/login";
  }
  return response
});

export default api;