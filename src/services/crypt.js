import CryptoJS from 'crypto-js';

export const encrypt = (value) => {
	return CryptoJS.AES.encrypt(value, process.env.REACT_APP_SECRET_KEY_CRYPT).toString();
}

export const decrypt = (value) => {
	return (CryptoJS.AES.decrypt(value.toString(), process.env.REACT_APP_SECRET_KEY_CRYPT)).toString(CryptoJS.enc.Utf8);
}