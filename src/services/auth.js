import api from './api';

export const isAuthenticated = () => {
	const token = localStorage.getItem('IOTREE_TOKEN');
	const refreshToken = localStorage.getItem('IOTREE_REFRESH_TOKEN');
	if(token && refreshToken) {
		return {
			token,
			refreshToken
		}
	} 
	return false;
}

export const isAdmin = async (id_user) => {
	let user = await api.get('/users/' + id_user);
	return user.is_admin;
}

export const getKeyConnection = () => {
	return localStorage.getItem('IOTREE_KEY_CONNECTION');
}

export const setKeyConnection = (value) => {
	localStorage.setItem('IOTREE_KEY_CONNECTION', value);
}

export const getToken = () => {
	const token = localStorage.getItem('IOTREE_TOKEN');
	const refreshToken = localStorage.getItem('IOTREE_REFRESH_TOKEN');
	return {
		token,
		refreshToken,
	}
}

export const getUser = () => {
	return JSON.parse(localStorage.getItem('IOTREE_USER'));
}

export const setUser = (user) => {
	localStorage.setItem('IOTREE_USER', JSON.stringify(user));
}

export const setToken = (token, refreshToken) => {
	localStorage.setItem('IOTREE_TOKEN', token);
	localStorage.setItem('IOTREE_REFRESH_TOKEN', refreshToken);
};

export const deleteToken = () => {

	let keys = Object.keys(localStorage);
	let i = keys.length;

    while ( i-- ) {
        localStorage.removeItem( keys[i] );
    }
};