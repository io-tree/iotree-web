import 'dotenv/config';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Routes from './routes';
import { Provider } from 'react-redux'
import store from './redux/store'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { SnackbarProvider } from 'notistack';


const theme = createMuiTheme({
  overrides: {
    MuiToolbar: {
      regular: {
        minHeight: 0,
        height: 50,
        '@media (min-height: 0px)': {
          minHeight: 0,
        }
      }
    }
  },
  status: {
    danger: 'black',
  },
  palette: {
    primary: {
      main: '#00B248'
    },
    secondary: {
      main: '#0088ff',
    },
  },
  typography: {
    fontFamily: [
      'Acme',
      'Courgette',
    ].join(','),
  }
});

const App = (
  <ThemeProvider theme={theme}>
    <SnackbarProvider dense maxSnack={4} autoHideDuration={4000} anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}>
      <Provider store={store}>
        <Routes />
      </Provider>
    </SnackbarProvider>
  </ThemeProvider>
)

ReactDOM.render(App, document.getElementById('root'));
