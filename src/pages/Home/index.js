import React, { useEffect, useState } from 'react';

import { 
	Container,
	Typography,
	Button
} from '@material-ui/core';

import socket from '../../services/socket';

import './styles.css';

export default ({history}) => {

	const [ msg, setMsg ] = useState('');

	useEffect(() => {
		socket.on('news', function (data) {
			console.log(socket.id);
			setMsg(data.hello);
			socket.emit('my other event', { my: 'data' });
		});
	},[]);
	
	return (
		<>
			<p>Home Page - {process.env.REACT_APP_URL_API} - {msg}</p>
			<Button
				variant='contained'
				color="primary"
				onClick={() => history.push('/login')}
			>
				Login
			</Button>
		</>
	)
};
