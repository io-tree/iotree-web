import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
	FaEye,
	FaEyeSlash
} from 'react-icons/fa';
import { useSnackbar } from 'notistack';
import useWindowDimensions  from '../../services/useWindowDimensions';
import { 
	Container,
	Typography,
	Button,
	Paper,
	Grid,
	Link,
	LinearProgress,
	Checkbox,
	FormControlLabel,
	TextField,
	InputAdornment,
	IconButton
} from '@material-ui/core';

import colors from '../../assets/colors';
import api from '../../services/api';
import auth from '../../services/auth';


const useStyles = makeStyles(theme => ({
	loginContainer: {
		height: '70%',
		[theme.breakpoints.down('xs')]: {
			width: '70%',
			padding: 0
		},
		[theme.breakpoints.up('sm')]: {
			width: '420px',
		}
	},
	loginPaperContainer: {
		padding: 40,
		position: 'relative',
		[theme.breakpoints.down('xs')]: {
			padding: 10
		},
	},
	loginTitle: {
		marginBottom: 25,
		marginTop: 15,
		[theme.breakpoints.down('xs')]: {
			fontSize: 20,
			marginTop: 10
		}
	},
	loginTextField: {
		width: '100%',
		marginBottom: 25,
	},
	loginTextFieldLabel: {
		[theme.breakpoints.down('xs')]: {
			fontSize: 14,
		}
	},
	loginButton: {
		width: '100%',
		marginTop: 30,
		backgroundColor: colors.primary,
		[theme.breakpoints.down('xs')]: {
			fontSize: 12,
		}
	},
	loginProgress: {
		position: 'absolute',
		width: '100%',
		top: 0,
		left: 0,
		borderTopLeftRadius: 3,
		borderTopRightRadius: 3,
		height: 7,
	}
  }));


export default ({history}) => {

	const classes = useStyles();
	const { enqueueSnackbar } = useSnackbar();

	const [ onSubmit, setOnSubmit ] = useState(false);
	const [ email, setEmail ] = useState('');


	function login() {
		setOnSubmit(true);
		api.post('/recoveryPassword', {
			email
		}).then( res => {
			console.log(res.data);
			let data = res.data;
			if(data.status === 'ok') {
				enqueueSnackbar(data.msg, { variant: 'success'})
			} else {
				if(data.code === 'validationError') {
					data.body.map( val => {
						enqueueSnackbar(val, { variant: 'error'})
					})
				} else {
					enqueueSnackbar(data.msg, { variant: 'error'})
				}
			}
			setOnSubmit(false);
		}).catch( err => {
			console.log('resultado error')
			console.log(err)
			setOnSubmit(false);
		})
	}

	return (
		<Container
			disableGutters
			maxWidth
			style={{
				position: 'fixed',
				height: '100%',
				backgroundColor: colors.primary,
				backgroundImage: `linear-gradient(315deg, ${colors.primary} 0%, ${colors.secondaryLight} 74%)`
			}}
		>
			<Grid
				wrap="nowrap"
				direction="row"
				justify="center"
				alignItems="center"
				container
				style={{
					height: '100%',
					//display: 'felx'
				}}
			>
				<Grid
					item
					className={classes.loginContainer}
				>
					<Paper
						elevation={10}
						className={classes.loginPaperContainer}
					>
						<Typography
							variant='h4'
							align='center'
							className={classes.loginTitle}
						>
							Redefinição de Senha
						</Typography>
						<TextField 
							id="standard-basic" 
							label={
								<Typography
									variant="inherit"
									className={classes.loginTextFieldLabel}
								>
									Email
								</Typography>
							} 
							helperText="Digite seu email"
							onChange={({target}) => setEmail(target.value)}
							className={classes.loginTextField}
							InputProps={{
								classes: {
									input: classes.loginTextFieldLabel
								}
							}}
						/>
						{
							onSubmit &&
							<LinearProgress 
								className={classes.loginProgress}
							/>
						}
						<Button
							variant="contained"
							disabled={onSubmit}
							onClick={login}
							color="secondary"
							className={classes.loginButton}
						>
							Enviar email
						</Button>
					</Paper>
				</Grid>
			</Grid>
		</Container>
	)
};
