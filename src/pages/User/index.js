import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { useSnackbar } from 'notistack';
import {
  Container,
  Typography,
  CircularProgress,
  Card,
  CardMedia,
  CardContent,
  CardActions,
  Grid,
  FormControlLabel,
  Switch,
  IconButton
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';

import {
  Fade
} from 'react-reveal';

import {
  FaUser,
  FaUserPlus,
  FaEllipsisV
} from 'react-icons/fa';

import api from '../../services/api';

const useStyles = makeStyles({
  card: {
    width: 220,
    boxShadow: '5px 5px 5px rgba(0,0,0,0.5)'
  },
  media: {
    height: 180
  }
});


export default ({ history }) => {

  const classes = useStyles();
  const dispatch = useDispatch()
  const isLoading = useSelector(state => state.isLoading)
  const { enqueueSnackbar } = useSnackbar();

  const [loading, setLoading] = useState(true);
  const [listUser, setListUser] = useState([]);

  useEffect(() => {
    getAllUsers();
  }, []);

  function getAllUsers () {
    api.get('/users').then(res => {
      let list = res.data.body.rows;

      setListUser(list);
      setLoading(false);
    })
  }

  function handleAdminMode (id_user, value) {
    api.put('/users', {
      id_user,
      is_admin: value
    }).then(res => {
      enqueueSnackbar(res.data.msg, { variant: 'info' })
      getAllUsers();
			/* let list = listUser;

			list = list.map( user => {
				if(user.id_user === id_user){
					user.is_admin = value;
				}
				return user
			});
			setListUser(list) */
    })
  }

  return (
    <Container
      disableGutters
      maxWidth={false}
      style={{
        padding: 15,
        borderRadius: 5,
        height: '100%',
      }}
    >
      {loading ?
        <Container style={{
          display: 'flex',
          justifyContent: 'center'
        }}>
          <CircularProgress />
        </Container>
        :
        <Grid
          container
          spacing={2}
        >{
            listUser.map((user, index) => {
              console.log(user)
              return (
                <Grid
                  item
                  key={index}
                >
                  <Fade
                    right
                    duration={100 * index}
                  >
                    <Card className={classes.card}>
                      <CardMedia
                        className={classes.media}
                        image={api.defaults.baseURL + user.path_img}
                      />
                      <CardContent>
                        <Typography gutterBottom style={{
                          padding: '0 10px',
                          whiteSpace: 'break-spaces'
                        }}>
                          {user.name}
                        </Typography>
                      </CardContent>
                      <CardActions>
                        <FormControlLabel
                          control={<Switch checked={user.is_admin} onChange={() => handleAdminMode(user.id_user, !user.is_admin)} color="primary" />}
                          label="Modo administrador"
                        />
                        <IconButton>
                          <FaEllipsisV />
                        </IconButton>
                      </CardActions>
                    </Card>
                  </Fade>
                </Grid>
              )
            })
          }</Grid>
      }
    </Container>
  )
};
