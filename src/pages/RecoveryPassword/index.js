import React, { useState, useEffect } from 'react';
import { 
	FaSave,
	FaRegEye,
	FaRegEyeSlash
} from 'react-icons/fa';

import qs from 'qs';
import { useSnackbar } from 'notistack';


import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';

import { 
	TextField,
	Grid,
	InputAdornment,
	IconButton,
	Container,
	Button,
	Typography
} from '@material-ui/core';

import api from '../../services/api';
import colors from '../../assets/colors';
import './styles.css'


export default (props) => {

	const { enqueueSnackbar } = useSnackbar();

	const [ showNewPassword, setShowNewPassword ] = useState(false);
	const [ showConfirmPassword, setShowConfirmPassword ] = useState(false);
	const [ newPassword, setNewPassword ]= useState(undefined);
	const [ confirmPassword, setConfirmPassword ]= useState(undefined);
	const [ nameUser, setNameUser ] = useState('');

	useEffect( () => {
		let { name } = qs.parse(props.location.search, { ignoreQueryPrefix: true });
		name = name.split(' ');
		if(name.length > 1) name = name[0] + ' ' + name[name.length - 1];
		else name = name[0]
		setNameUser(name)
	}, [])

	function onSubmit() {


		if(newPassword === undefined || newPassword !== confirmPassword) {
			enqueueSnackbar('Senhas não correspondentes', { variant: 'error' } );
			return;
		}

		api.put('/recoveryPassword', {
			hash: props.match.params.hash,
			password: newPassword
		}).then( res => {
			console.log(res.data)
			if(res.data.status === 'ok') {
				enqueueSnackbar(res.data.msg, { variant: 'success' } );
			} else {
				enqueueSnackbar(res.data.msg, { variant: 'error' } );
			}
		}).catch( err => {
			console.log(err)
		})
	}

	return (
		<Container maxWidth={'xm'} style={{
			position: 'fixed',
			height: '100%',
			backgroundImage: `linear-gradient(315deg, ${colors.primary} 0%, ${colors.secondaryLight} 94%)`,
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'center',
			flexDirection: 'column',
		}}>
			<Container>
				<Slide right cascade>
					<div>
						<Typography noWrap={false} variant={'h4'} align={'center'} style={{
							color: 'white',
							fontWeight: 'Bold',
						}}>
							{nameUser},
						</Typography>
						<Typography style={{
							color: 'white',
							fontSize: 18,
							wordWrap: 'break-word',
							textAlign: 'center',
							whiteSpace: 'normal'
						}}>
							digite sua nova senha no campo abaixo
						</Typography>
					</div>
				</Slide>
			</Container>
			<Container style={{
				width: 'auto',
				marginTop: 20,
			}}>
				<Fade bottom big>
					<Grid container spacing={3} style={{
						backgroundColor: 'white',
						padding: 10,
						display: 'flex',
						flexDirection: 'column',
						width: 'auto',
						borderRadius: 10,
						boxShadow: "8px 8px 8px rgba(0,0,0,0.4)"
					}}>
						<Grid item >
							<TextField 
								color={'secondary'}
								id="input-with-icon-grid" 
								label="Nova senha" 
								required
								onChange={ text => setNewPassword(text.target.value)}
								value={newPassword}
								type={showNewPassword ? 'text' : 'password'} 
								InputProps={{
									endAdornment: (
										<InputAdornment position="end">
											<IconButton
												aria-label="toggle password visibility"
												onClick={() => setShowNewPassword(!showNewPassword)}
												onMouseDown={() => setShowNewPassword(false)}
											>
												{showNewPassword ? <FaRegEye /> : <FaRegEyeSlash />}
											</IconButton>
										</InputAdornment>
									)
								}}
							/>
						</Grid>
						<Grid item >
							<TextField 
								color={'secondary'}
								id="input-with-icon-grid" 
								label="Confirme a senha"  
								type={showConfirmPassword ? 'text' : 'password'} 
								value={confirmPassword}
								required
								onChange={text => setConfirmPassword(text.target.value)}
								InputProps={{
									endAdornment: (
										<InputAdornment position="end">
											<IconButton
												aria-label="toggle password visibility"
												onClick={() => setShowConfirmPassword(!showConfirmPassword)}
												onMouseDown={() => setShowConfirmPassword(false)}
											>
												{showConfirmPassword ? <FaRegEye /> : <FaRegEyeSlash />}
											</IconButton>
										</InputAdornment>
									)
								}}
							/>
						</Grid>
						<Button
							variant="contained"
							startIcon={<FaSave />}
							onClick={onSubmit}
							color={'secondary'}
							style={{
								color: 'white',
								marginTop: 20,
							}}
						>
							Salvar
						</Button>
					</Grid>
				</Fade>
			</Container>
		</Container>
	)
}