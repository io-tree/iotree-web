import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import useWindowDimensions from '../../services/useWindowDimensions';
import { Scrollbars } from 'react-custom-scrollbars';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
  Flip,
  Fade,
  Zoom
} from 'react-reveal';
import { useSnackbar } from 'notistack';
import {
  Container,
  Drawer,
  IconButton,
  Divider,
  AppBar,
  Toolbar,
  Typography,
  List,
  ListItemIcon,
  ListItemText,
  ListItem,
  Grid,
  Avatar,
  LinearProgress,
} from '@material-ui/core';
import {
  FaAngleLeft,
  FaAngleRight,
  FaHome,
  FaSignOutAlt,
  FaUser,
  FaSeedling,
  FaCube,
  FaUpload,
  FaListAlt
} from 'react-icons/fa';
import { useSelector, useDispatch } from 'react-redux'

import colors from '../../assets/colors';
import {
  getUser,
  deleteToken,
  isAuthenticated
} from '../../services/auth';

import useStyles from './styles'
import api from '../../services/api';
import { appActions } from '../../redux'

import DashboardHome from '../Graphic';
import DashboardUsers from '../User';
import DashboardGreenhouse from '../Greenhouse';
import DashboardPlant from '../Plant';
import DashboardCategory from '../Category'
import DashboardUpload from '../Upload'

const drawerWidth = 200;
const headerHeight = 50;

const menuList = [
  /*   {
      icon: <FaHome />,
      text: "Inicio",
      page: <DashboardHome />
    }, */
  {
    icon: <FaUser />,
    text: "Usuários",
    page: <DashboardUsers />
  },
  {
    icon: <FaListAlt />,
    text: "Categorias",
    page: <DashboardCategory />
  },
  {
    icon: <FaSeedling />,
    text: "Plantas",
    page: <DashboardPlant />
  },
  {
    icon: <FaCube />,
    text: "Estufas",
    page: <DashboardGreenhouse />
  },
  {
    icon: <FaUpload />,
    text: "Uploads",
    page: <DashboardUpload />
  },
]

export default ({ history, location, match }) => {

  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch()
  const isLoading = useSelector(state => state.isLoading)
  const classes = useStyles();
  const theme = useTheme();

  const [open, setOpen] = useState(true);
  const [user, setUser] = useState(getUser());
  const [title, setTitle] = useState('Home');
  const [page, setPage] = useState('');
  const [indexPage, setIndexPage] = useState(2);
  const [resetReveal, setResetReveal] = useState(true);

  useEffect(() => {
    setPage(menuList[indexPage].page);
    setTitle(menuList[indexPage].text);
    let user = getUser();

    let name = user.name.split(' ');
    if (name.length > 1) {
      name = name[0] + ' ' + name[name.length - 1];
    } else {
      name = name[0]
    }
    user.name = name;
    setUser(user);
    dispatch(appActions.setUser(user))
    dispatch(appActions.setLoadingDashboard(false))
  }, [dispatch, indexPage]);

  function logOut () {
    deleteToken();
    enqueueSnackbar('You was disconnected successfully', { variant: 'info' })
    history.push('/login')
  }

  return (
    <Container
      disableGutters
      maxWidth={false}
      style={{
        position: 'fixed',
        height: '100%',
        backgroundColor: colors.primaryDark,
        backgroundImage: `linear-gradient(to top, ${colors.grayLight}, ${colors.grayLight} 70%, transparent 0)`
      }}
    >
      <AppBar
        position="fixed"
        elevation={0}
        className={classes.appBar}
      >
        {isLoading &&
          <LinearProgress classes={{
            colorPrimary: classes.loadingBar
          }} />
        }
        <Toolbar
          disableGutters
          className={classes.toolbarTitle}
        >
          <Typography
            variant="h6"
            noWrap
            style={{
              width: drawerWidth,
              textAlign: 'center',
              flexShrink: 0,
            }}
          >
            IoTree
					</Typography>
          <Grid
            container
            style={{
              display: 'flex',
              alignItems: 'center',
              height: headerHeight
            }}
          >
            <Grid
              item
              xs
              style={{
                display: 'flex',
                justifyContent: 'flex-start'
              }}
            >

            </Grid>
            <Grid
              item
              xl={6}
              style={{
                display: 'flex',
                justifyContent: 'center'
              }}
            >
              <Typography component="div">
                <Zoom
                  top
                  cascade
                  spy={resetReveal}
                >
                  {title}
                </Zoom>
              </Typography>
            </Grid>
            <Grid
              xs
              item
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              <IconButton
                style={{
                  color: 'white'
                }}
                onClick={logOut}
              >
                <FaSignOutAlt />
              </IconButton>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Grid
        container
        style={{
          position: 'absolute',
          bottom: 0,
          right: 0,
          height: `calc(100% - ${headerHeight}px)`
        }}
      >
        <Grid
          item
        >
          <Drawer
            variant="permanent"
            className={clsx(classes.drawer, {
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            })}
            classes={{
              paper: clsx(classes.drawerPaper, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
              }),
              paperAnchorDockedLeft: makeStyles({ rr: { border: 0 } })().rr
            }}
          >
            <Container
              disableGutters
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column'
              }}
            >
              <IconButton>
                <Avatar
                  src={api.defaults.baseURL + user.path_img}
                  className={clsx(classes.iconUser, {
                    [classes.iconUserSmall]: !open
                  })}
                />
              </IconButton>
              <Typography
                className={clsx({
                  [classes.hide]: !open
                })}
                style={{
                  paddingBottom: 10,
                  textAlign: 'center',
                  fontSize: 15
                }}
              >
                {user.name}
              </Typography>
              {user.is_admin &&
                <Typography
                  className={clsx({
                    [classes.hide]: !open
                  })}
                  style={{
                    paddingBottom: 10,
                    textAlign: 'center',
                    fontSize: 10
                  }}
                >
                  Administrador
								</Typography>
              }
            </Container>
            <Divider />
            <List>
              {menuList.map((menu, index) => (
                <ListItem
                  button
                  key={index}
                  className={clsx(classes.drawerListItem, {
                    [classes.drawerListItemSelected]: (index === indexPage)
                  })}
                  onClick={() => {
                    if (index != indexPage) {
                      setPage(menu.page);
                      setTitle(menu.text);
                      setResetReveal(true);
                      setIndexPage(index);
                    }
                  }}
                >
                  <ListItemIcon
                    className={classes.drawerListIcon}
                    style={{
                      color: (index === indexPage ? "white" : "")
                    }}
                  >
                    {menu.icon}
                  </ListItemIcon>
                  <ListItemText
                    primary={menu.text}
                    className={clsx(classes.drawerListText, {
                      [classes.hide]: !open
                    })}
                  />
                </ListItem>
              ))}
            </List>
            <Container
              disableGutters
              className={classes.containerButtonHandle}
            >
              <IconButton
                onClick={() => setOpen(!open)}
                className={clsx(classes.buttonHandle, {
                  [classes.buttonHandleClose]: !open
                })}
              >
                {theme.direction === 'rtl' ? <FaAngleRight /> : <FaAngleLeft />}
              </IconButton>
            </Container>
          </Drawer>
        </Grid>
        <Grid
          item
          className={classes.contentPage}
        >
          <Scrollbars>
            <Fade
              right
              opposite
              duration={350}
              onReveal={() => setResetReveal(false)}
              spy={resetReveal}
            >
              <Container
                maxWidth={false}
              >
                {page}
              </Container>
            </Fade>
          </Scrollbars>
        </Grid>
      </Grid>
    </Container>
  )
};
