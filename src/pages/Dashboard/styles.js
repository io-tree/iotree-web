import { makeStyles } from '@material-ui/core/styles';
import colors from '../../assets/colors';

const drawerWidth = 200;
const iconWidth = 60;
const headerHeight = 50;
const listItemHeeight = 35;

export default makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: 'transparent',
    height: headerHeight
  },
  hide: {
    display: 'none',
  },
  drawer: {
    height: '100%',
    justifyContent: 'center',
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerPaper: {
    overflow: 'hidden',
    height: `calc(100% - ${headerHeight}px)`,
    bottom: 0,
    top: 'unset',
    borderTopRightRadius: 10,
    boxShadow: '5px 5px 8px rgba(0,0,0,0.5)'
  },
  iconUser: {
    width: iconWidth + 20,
    height: iconWidth + 20,
    transition: theme.transitions.create(['width', 'height'], {
      easing: theme.transitions.easing.sharp,
      duration: 280,
    }),
  },
  iconUserSmall: {
    width: iconWidth - 20,
    height: iconWidth - 20,
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: iconWidth,
    },
  },
  drawerList: {
    display: 'flex',
    justifyContent: 'left'
  },
  drawerListItem: {
    display: 'flex',
    justifyContent: 'flex-start',
    height: listItemHeeight,
    padding: 0,
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.15)',
    },
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: 380,
    }),
  },
  drawerListItemSelected: {
    backgroundColor: colors.primaryDark,
    borderRadius: 5,
    margin: 'auto',
    boxShadow: '5px 5px 5px rgba(0,0,0,0.45)',
    color: 'white',
    '&:hover': {
      backgroundColor: colors.primaryDark,
      opacity: 0.75,
    },
  },
  drawerListIcon: {
    minWidth: 'auto',
    width: iconWidth,
    display: 'flex',
    justifyContent: 'center'
  },
  drawerListText: {
    paddingLeft: 0
  },
  toolbarTitle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    color: 'white'
  },
  toolbarHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%',
    minHeight: 0
  },
  containerButtonHandle: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: iconWidth,
    display: 'flex',
    justifyContent: "center"
  },
  buttonHandle: {
    transition: theme.transitions.create(['transform'], {
      easing: theme.transitions.easing.sharp,
      duration: 280,
    }),
  },
  buttonHandleClose: {
    justifyContent: 'center',
    transform: `rotate(180deg)`
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  loadingBar: {
    backgroundColor: 'white',
    position: 'absolute',
    width: '100%',
    top: 0,
    left: 0
  },
  contentPage: {
    display: 'block',
    paddingTop: 0,
    paddingBottom: 10,
    flex: 1,
    overflow: 'hidden',
    height: `calc(100% - ${headerHeight})`
    //height: useWindowDimensions().height - 
  }
}));