import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  FaEye,
  FaEyeSlash
} from 'react-icons/fa';
import { useSnackbar } from 'notistack';
import {
  Container,
  Typography,
  Button,
  Paper,
  Grid,
  Link,
  LinearProgress,
  Checkbox,
  FormControlLabel,
  TextField,
  InputAdornment,
  IconButton
} from '@material-ui/core';

import colors from '../../assets/colors';
import api from '../../services/api';
import {
  getToken,
  setToken,
  setKeyConnection,
  getKeyConnection,
  setUser,
} from '../../services/auth';


const useStyles = makeStyles(theme => ({
  loginContainer: {
    height: '70%',
    [theme.breakpoints.down('xs')]: {
      width: '70%',
      padding: 0
    },
    [theme.breakpoints.up('sm')]: {
      width: '420px',
    }
  },
  loginPaperContainer: {
    padding: 40,
    height: '100%',
    position: 'relative',
    [theme.breakpoints.down('xs')]: {
      padding: 20
    },
  },
  loginTitle: {
    marginBottom: 25,
    marginTop: 15,
    [theme.breakpoints.down('xs')]: {
      fontSize: 25,
      marginTop: 10
    }
  },
  loginTextField: {
    width: '100%',
    marginBottom: 25,
  },
  loginTextFieldLabel: {
    [theme.breakpoints.down('xs')]: {
      fontSize: 14,
    }
  },
  loginTextStayConnected: {
    textAlign: 'left',
    [theme.breakpoints.down('xs')]: {
      fontSize: 12,
    }
  },
  loginButton: {
    width: '100%',
    marginTop: 30,
    backgroundColor: colors.primary,
    [theme.breakpoints.down('xs')]: {
      fontSize: 12,
    }
  },
  loginProgress: {
    position: 'absolute',
    width: '100%',
    top: 0,
    left: 0,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    height: 7,
  },
  loginButtonForgot: {
    marginTop: 20,
    padding: 0,
    textAlign: 'center',
    [theme.breakpoints.down('xs')]: {
      fontSize: 12,
    }
  },
  loginButtonLink: {
    position: 'absolute',
    bottom: 15,
    left: 0,
    width: '100%',
    padding: 0,
    textAlign: 'center',
    [theme.breakpoints.down('xs')]: {
      fontSize: 12,
    }
  }
}));


export default ({ history }) => {

  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [onSubmit, setOnSubmit] = useState(true);
  const [stayConnectedChecker, setStayConnectedChecker] = useState(true);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [viewPassword, setViewPassword] = useState(false);

  useEffect(() => {
    const keyConnection = getKeyConnection();
    if (keyConnection) {
      api.post('/reconnect', {
        keyConnection
      }).then(res => {
        let data = res.data;

        let name = data.body.name.split(' ');
        if (name.length > 1) {
          name = name[0] + ' ' + name[name.length - 1];
        } else {
          name = name[0]
        }

        setKeyConnection(data.keyConnection);
        setToken(data.token, data.refreshToken);
        setUser(data.body);
        enqueueSnackbar('Bem vindo de volta ' + name, { variant: 'success' });
        history.push('/dashboard/home');
        setOnSubmit(false);
      }).catch(err => {
        setOnSubmit(false);
      })
    } else {
      setOnSubmit(false);
    }
  }, [enqueueSnackbar, history])


  function login () {
    setOnSubmit(true);
    api.post('/sessions', {
      email,
      password,
      stayConnected: stayConnectedChecker
    }).then(res => {
      let data = res.data;
      if (data.status === 'ok') {
        if (stayConnectedChecker)
          setKeyConnection(data.keyConnection);
        setToken(data.token, data.refreshToken);
        setUser(data.body);
        enqueueSnackbar(data.msg, { variant: 'success' });
        history.push('/dashboard/home')
      } else {
        if (data.code === 'validationError') {
          data.body.map(val => {
            enqueueSnackbar(val, { variant: 'error', anchorOrigin: { vertical: 'top', horizontal: 'right' } });
          })
        } else if (data.code === 'userVerifyEmail') {
          enqueueSnackbar(data.msg, { variant: 'info' })
        } else {
          enqueueSnackbar(data.msg, { variant: 'error' })
        }
      }
      setOnSubmit(false);
    }).catch(err => {
      console.log('resultado error')
      console.log(err)
      setOnSubmit(false);
    })
  }

  return (
    <Container
      disableGutters
      maxWidth={false}
      style={{
        position: 'fixed',
        height: '100%',
        backgroundColor: colors.primary,
        backgroundImage: `linear-gradient(315deg, ${colors.primary} 0%, ${colors.secondaryLight} 94%)`
      }}
    >
      <Grid
        wrap="nowrap"
        direction="row"
        justify="center"
        alignItems="center"
        container
        style={{
          height: '100%',
        }}
      >
        <Grid
          item
          className={classes.loginContainer}
        >
          <Paper
            elevation={10}
            className={classes.loginPaperContainer}
          >
            <Typography
              variant='h4'
              align='center'
              className={classes.loginTitle}
            >
              Login
						</Typography>
            <TextField
              id="standard-basic"
              label={
                <Typography
                  variant="inherit"
                  className={classes.loginTextFieldLabel}
                >
                  Email
								</Typography>
              }
              onChange={({ target }) => setEmail(target.value)}
              className={classes.loginTextField}
              InputProps={{
                classes: {
                  input: classes.loginTextFieldLabel
                }
              }}
            />
            <TextField
              id="standard-basic"
              label={
                <Typography
                  variant="inherit"
                  className={classes.loginTextFieldLabel}
                >
                  Senha
								</Typography>
              }
              type={viewPassword ? "text" : "password"}
              onChange={({ target }) => setPassword(target.value)}
              className={classes.loginTextField}
              InputProps={{
                classes: {
                  input: classes.loginTextFieldLabel
                },
                endAdornment: (
                  <InputAdornment
                    position="end"
                  >
                    <IconButton onClick={() => setViewPassword(!viewPassword)}  >
                      {viewPassword ?
                        <FaEye />
                        :
                        <FaEyeSlash />
                      }
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={stayConnectedChecker}
                  onChange={() => setStayConnectedChecker(!stayConnectedChecker)}
                  color="primary"
                />
              }
              fontSize={2}
              label={
                <Typography
                  variant="inherit"
                  align="left"
                  className={classes.loginTextStayConnected}
                >
                  Manter conectado
								</Typography>
              }
            />
            {
              onSubmit &&
              <LinearProgress
                className={classes.loginProgress}
              />
            }
            <Button
              variant="contained"
              disabled={onSubmit}
              onClick={login}
              color="secondary"
              className={classes.loginButton}
            >
              Enviar
						</Button>
            <Typography
              className={classes.loginButtonForgot}
            >
              Esqueceu sua senha?
							<Link
                onClick={() => {
                  history.push('/password/forgot')
                }}
                style={{
                  marginLeft: 10,
                  cursor: 'pointer'
                }}
              >
                Clique aqui
							</Link>
            </Typography>
            {/* <Typography
							className={classes.loginButtonLink}
						>
							Não é membro?
							<Link
								onClick={() => {
									console.info("I'm a button.");
								}}
								style={{
									marginLeft: 10,
									cursor: 'pointer'
								}}
							>
								Cadastrar-se 
							</Link>
						</Typography> */}
          </Paper>
        </Grid>
      </Grid>
    </Container>
  )
};
