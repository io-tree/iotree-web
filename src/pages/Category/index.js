import React, { useState, useEffect, useCallback } from 'react';
import moment from 'moment'
import { useSnackbar } from 'notistack';
import {
  Container,
} from '@material-ui/core';

import { useSelector, useDispatch } from 'react-redux'
import { appActions } from '../../redux'
import api from '../../services/api';
import useStyles from './styles'
import MaterialTable from '../../components/MaterialTable'

export default ({ history }) => {

  const classes = useStyles();
  const dispatch = useDispatch()
  const isLoading = useSelector(state => state.isLoading)
  const { enqueueSnackbar } = useSnackbar();
  const [listCategory, setListCategory] = useState([]);

  useEffect(() => {
    dispatch(appActions.setLoadingDashboard(true))
    api.get('/categories').then(res => {
      let data = res.data;
      if (data.status === 'ok') {
        const list = data.body
        const temp = []
        list.map(item => {
          temp.push({
            ...item,
            createdAt: moment(new Date(item.createdAt)).format('DD/MM/YYYY - hh:mm'),
          })
        })
        setListCategory(temp);
        dispatch(appActions.setLoadingDashboard(false))
      }
    })
  }, [dispatch]);

  const addRow = (newData) => {
    return api.post('/categories', {
      name: newData.name,
      num_order: listCategory.length + 1
    }).then(res => {
      const item = {
        ...res.data.body,
        createdAt: moment(new Date(res.data.body.createdAt)).format('DD/MM/YYYY - hh:mm'),
      }
      setListCategory([...listCategory, item])
    })
  }

  const updateRow = (newData, oldData) => {
    return api.put(`/categories/${newData.id_category}`, {
      name: newData.name,
    }).then(res => {
      const dataUpdate = [...listCategory];
      const index = dataUpdate.findIndex(data => data.id_category === oldData.id_category)
      dataUpdate[index] = newData;
      setListCategory([...dataUpdate]);
    })
  }

  const deleteRow = (oldData) => {
    return api.delete('/categories/' + oldData.id_category).then(res => {
      const newList = listCategory.filter(item => item.id_category !== oldData.id_category)
      setListCategory([...newList])
    })
  }

  return (
    <Container
      disableGutters
      maxWidth={false}
      style={{
        padding: 15,
        borderRadius: 5,
        height: '100%',
      }}
    >
      {!isLoading &&
        <MaterialTable
          columns={[
            { title: 'Nome', field: 'name' },
            { title: 'Criado em', field: 'createdAt', editable: 'never' }
          ]}
          title="Categorias"
          data={listCategory}
          isDeletable={rowData => (rowData.id_category > 1)}
          isDeleteHidden={rowData => !(rowData.id_category > 1)}
          onRowAdd={addRow}
          onRowUpdate={updateRow}
          onRowDelete={deleteRow}
        />
      }
    </Container>
  )
};
