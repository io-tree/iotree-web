import React, { useState, useEffect, useCallback } from 'react';
import clsx from 'clsx';
import moment from 'moment'
import { useSnackbar } from 'notistack';
import {
  Container,
  Grid,
} from '@material-ui/core';

import { useSelector, useDispatch } from 'react-redux'
import { appActions } from '../../redux'
import api from '../../services/api';
import useStyles from './styles'
import MaterialTable from '../../components/MaterialTable'

export default ({ history }) => {

  const classes = useStyles();
  const dispatch = useDispatch()
  const isLoading = useSelector(state => state.isLoading)
  const { enqueueSnackbar } = useSnackbar();
  const [listGreenhouse, setListGreenhouse] = useState([]);
  const [listGreenhouseType, setListGreenhouseType] = useState([]);

  const getAllGreenhouses = useCallback(() => {
    api.get('/greenhouses').then(res => {
      let data = res.data;
      if (data.status === 'ok') {
        const list = data.body
        const temp = []
        list.map(item => {
          temp.push({
            ...item,
            createdAt: moment(new Date(item.createdAt)).format('DD/MM/YYYY - hh:mm'),
          })
        })
        setListGreenhouse(temp);
      }
    })
  }, [])

  const getAllGreenhouseTypes = useCallback(() => {
    api.get('/greenhouse_types').then(res => {
      let temp = {}
      res.data.body.map(item => {
        temp[item.id_greenhouse_type] = item.name
      })
      setListGreenhouseType(temp);
    })
  }, [])

  useEffect(() => {
    dispatch(appActions.setLoadingDashboard(true))
    Promise.all([getAllGreenhouses(), getAllGreenhouseTypes()]).then(res => {
      dispatch(appActions.setLoadingDashboard(false))
    })
  }, [dispatch, getAllGreenhouseTypes, getAllGreenhouses]);

  const addRow = (newData) => {
    return api.post('/greenhouses', {
      name: newData.name,
      id_type: newData.id_type
    }).then(res => {
      const item = {
        ...res.data.body,
        createdAt: moment(new Date(res.data.body.createdAt)).format('DD/MM/YYYY - hh:mm'),
      }
      setListGreenhouse([...listGreenhouse, item])
    })
  }

  const updateRow = (newData, oldData) => {
    return api.put(`/greenhouses/${newData.category_id}`, {
      name: newData.name,
      description: newData.description
    }).then(res => {
      const dataUpdate = [...listGreenhouse];
      const index = dataUpdate.findIndex(data => data.category_id === oldData.category_id)
      dataUpdate[index] = newData;
      setListGreenhouse([...dataUpdate]);
    })
  }

  return (
    <Container
      disableGutters
      maxWidth={false}
      style={{
        padding: 15,
        borderRadius: 5,
        height: '100%',
      }}
    >
      {!isLoading &&
        <Grid
          container
          spacing={2}
        >
          <MaterialTable
            columns={[
              { title: 'Código', field: 'code', editable: 'never' },
              { title: 'Nome', field: 'name', editable: 'never' },
              { title: 'Tipo', field: 'id_type', lookup: listGreenhouseType },
              { title: 'Criado em', field: 'createdAt', editable: 'never' }
            ]}
            title="Estufas"
            data={listGreenhouse}
            onRowAdd={addRow}
          //onRowUpdate={updateRow}
          //onRowDelete={deleteRow}
          />
        </Grid>
      }
    </Container>
  )
};
