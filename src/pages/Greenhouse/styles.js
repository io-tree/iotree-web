import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  card: {
    width: 220,
    boxShadow: '5px 5px 5px rgba(0,0,0,0.5)'
  },
  media: {
    height: 180
  },
  gridItem: {
    marginBottom: 30,
    [theme.breakpoints.down('xs')]: {
      width: '70%',
      padding: 0
    },
    [theme.breakpoints.up('sm')]: {
      width: '450px',
    }
  },
  gridSlider: {
    border: `1px solid ${theme.palette.divider}`,
    position: 'relative',
    padding: 8,
    '& > p': {
      //color: theme.palette.primary.main,
      position: 'absolute',
      top: -12,
      padding: '0 10px',
      backgroundColor: theme.palette.background.paper
    }
  },
  slider: {
    '& > span': {
      padding: 5,
    },
    '& span > span': {
      color: 'white',
      whiteSpace: 'nowrap'
    }
  }
}));