import React from 'react';

import { 
	Container,
	Typography,
	Button
} from '@material-ui/core';

export default ({history}) => {
	
	return (
		<Container
			disableGutters
			maxWidth={'xl'}
		>
			<Typography>Home Dashboard</Typography>
		</Container>
	)
};
