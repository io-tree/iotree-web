import React, { useState, useEffect } from 'react';
import { 
	Container,
	Typography
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Lottie from 'react-lottie';
import api from '../../services/api';

import loading from '../../assets/loading.json';
import './styles.css';
import colors from '../../assets/colors';

const useStyles = makeStyles({
	root: {
	  background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
	  border: 0,
	  borderRadius: 3,
	  boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
	  color: 'white',
	  height: 48,
	  padding: '0 30px',
	},
	text: {
		color: 'white',
		paddingTop: 10,
		paddingBottom: 10
	}
  });

export default (props) => {

	const classes = useStyles();

	const [ isVerify, setIsVerify ] = useState(0); // 0 - page loading, 1 - page loaded, 2 - email verified, -1 - email verified error
	const [ language, setLanguage ] = useState('pt-br');
	const [ msg, setMsg ] = useState('');
	const [ title, setTitle ] = useState('');
	const [ msgClosePage, setMsgClosePage ] = useState('');

	useEffect( () => {
		/* api.get('/words').then( res => {
			let serverMsg = res.data.body.localeText.message;
			setTitle(serverMsg.emailCheckerTitle[language]);
			setMsg(serverMsg.emailChecking[language]);
			setMsgClosePage(serverMsg.emailClosePage[language]);
			setIsVerify(1)
		}); */
		setTimeout(() => {
			let temp = props.match.params.hash;
			api.put('/verification/' + temp).then( res => {
				if(res.data.status === 'ok') {
					setIsVerify(2);
					setMsg(res.data.msg);
				} else {
					setIsVerify(-1)
				}
				console.log(res)
			})
		}, 2000)
	}, []);


	//console.log(qs.parse(props.location.search, { ignoreQueryPrefix: true }))
	return (
		<Container maxWidth={'xm'} style={{
			position: 'fixed',
			height: '100%',
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			backgroundColor: '#e6e6e6'
		}}>
			<Container maxWidth={'sm'} style={{
				backgroundColor: '#00B248',
				borderRadius: 10,
				boxShadow: "8px 8px 8px rgba(0,0,0,0.4)"
			}}>
				
				<Container style={{
					padding: 10,
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
				}}>
					{isVerify !== 0 &&
						<Typography style={{
							fontSize: 25,
							color: 'white'
						}}>
							{title}
						</Typography>
					}
				</Container>
				<div className={'content-body'}>
					{isVerify === 0 ? 
						<>
							<Lottie 
								options={{
									loop: true,
									autoplay: true, 
									animationData: loading,
									rendererSettings: {
										preserveAspectRatio: 'xMidYMid slice'
									}
								}}
								speed={2}
								width={200}
								height={200}
							/>
						</>
					: isVerify === 1 ?
						<>
							<Lottie 
								options={{
									loop: true,
									autoplay: true, 
									animationData: loading,
									rendererSettings: {
										preserveAspectRatio: 'xMidYMid slice'
									}
								}}
								speed={2}
								width={200}
								height={200}
							/>
							<Typography className={classes.text}>{msg}</Typography>
						</>
					: isVerify === 2 ?
						<>
							<Typography className={classes.text}>{msg}</Typography>
							<Typography className={classes.text}>{msgClosePage}</Typography>
						</>
					: isVerify === -1 &&
						<>
							<Typography className={classes.text}>{msg}</Typography>
							<Typography className={classes.text}>{msgClosePage}</Typography>
						</>
					}
				</div>
			</Container>
		</Container>
	)
}