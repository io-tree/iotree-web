import React, { useState, useEffect, useCallback } from 'react';
import clsx from 'clsx';
import { useSnackbar } from 'notistack';
import {
  Container,
  Typography,
  Card,
  CardMedia,
  CardContent,
  CardActions,
  Grid,
  InputLabel,
  FormControl,
  FormControlLabel,
  Switch,
  IconButton,
  Slider,
  TextField,
  Select,
  MenuItem
} from '@material-ui/core';

import {
  Fade,
} from 'react-reveal';

import {
  FaEllipsisV, FaTrash,
} from 'react-icons/fa';
import { useSelector, useDispatch } from 'react-redux'
import { appActions } from '../../redux'
import api from '../../services/api';
import useStyles from './styles'

import FloatingAddButton from '../../components/FloatingAddButton';
import DialogAdd from '../../components/DialogAdd';

export default ({ history }) => {

  const classes = useStyles();
  const dispatch = useDispatch()
  const isLoading = useSelector(state => state.isLoading)
  const { enqueueSnackbar } = useSnackbar();
  const [listPlant, setListPlant] = useState([]);
  const [listCategories, setListCategories] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [createLoading, setCreateLoading] = useState(false);

  const [name, setName] = useState('');
  const [scientificName, setScientificName] = useState('');
  const [description, setDescription] = useState('');
  const [family, setFamily] = useState('');
  const [origin, setOrigin] = useState('');
  const [luminosity, setLuminosity] = useState('');
  const [weather, setWeather] = useState('');
  const [idCategory, setIdCategory] = useState(1);
  const [humidityAir, setHumidityAir] = useState([0, 100]);
  const [humidityGround, setHumidityGround] = useState([0, 100]);
  const [cycle, setCycle] = useState([0, 366]);
  const [temperature, setTemperature] = useState([0, 100]);
  const [pH, setpH] = useState([0, 100]);
  const [height, setHeight] = useState([0, 100]);

  const getAllPlant = useCallback(() => {
    api.get('/plants').then(res => {
      let data = res.data;
      if (data.status === 'ok') {
        setListPlant(data.body);
        //globalActions.setLoading(false)
      }
    })
  }, [])

  const getCategoriesPlant = useCallback(() => {
    api.get('/categories').then(res => {
      setListCategories(res.data.body)
    })
  }, [])

  useEffect(() => {
    dispatch(appActions.setLoadingDashboard(true))
    Promise.all([getCategoriesPlant(), getAllPlant()]).then(res => {
      dispatch(appActions.setLoadingDashboard(false))
    })
  }, [dispatch, getAllPlant, getCategoriesPlant]);

  async function createPlant () {
    setCreateLoading(!createLoading);

    api.post('/plants', {
      name,
      scientific_name: scientificName,
      description,
      id_category: idCategory,
      //family,
      //origin,
      //luminosity,
      //weather,
      'humidity_air': {
        min: humidityAir[0],
        max: humidityAir[1]
      },
      'humidity_ground': {
        min: humidityGround[0],
        max: humidityGround[1]
      },
      "cycle": {
        min: cycle[0],
        max: cycle[1]
      },
      "temperature": {
        min: temperature[0],
        max: temperature[1]
      },
      "pH": {
        min: pH[0],
        max: pH[1]
      },
      "height": {
        min: height[0],
        max: height[1]
      }
    }).then(res => {
      setCreateLoading(false);
      if (res.data.status === 'ok') {
        //globalActions.setLoading(true);
        setOpenDialog(false);
        //globalActions.setLoading(true);
        getAllPlant();
      } else {
        res.data.body.map(msg => {
          enqueueSnackbar(msg, { variant: 'error' })
        })
      }
    })
  }

  const onDelete = (plantId) => {
    api.delete('/plants/' + plantId).then(res => {
      enqueueSnackbar('Planta deletada', {
        variant: 'success'
      })
      getAllPlant();
    })
  }


  return (
    <Container
      disableGutters
      maxWidth={false}
      style={{
        padding: 15,
        borderRadius: 5,
        height: '100%',
      }}
    >
      {!isLoading &&
        <>

          <Grid
            container
            spacing={2}
          >{
              listPlant.map((plant, index) => {
                console.log(plant)
                return (
                  <Grid
                    item
                    key={index}
                  >
                    <Fade
                      right
                      duration={100 * index}
                    >
                      <Card className={classes.card}>
                        <CardMedia
                          className={classes.media}
                          image={api.defaults.baseURL + plant.path_img}
                        />
                        <CardContent>
                          <Typography gutterBottom variant="body2">
                            {plant.name}
                          </Typography>
                          <Typography variant="body2" color="textSecondary" component="p">
                            {plant.category.name}
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <IconButton
                            onClick={() => onDelete(plant.id_plant)}
                          >
                            <FaTrash />
                          </IconButton>
                        </CardActions>
                      </Card>
                    </Fade>
                  </Grid>
                )
              })
            }</Grid>
          <FloatingAddButton label={'Adicionar planta'} onClick={() => setOpenDialog(true)} />
        </>
      }
      <DialogAdd
        onOpen={openDialog}
        onClose={setOpenDialog}
        onClickSave={createPlant}
        label={'Adicionar planta'}
        loading={createLoading}
      >
        <Grid
          container
          justify="center"
          alignItems="center"
          direction="column"
          style={{
            marginTop: 20,
          }}
        >
          <Grid item className={classes.gridItem}>
            <TextField
              label="nome"
              fullWidth
              onChange={({ target }) => setName(target.value)}
              helperText="Digite o nome da planta"
            />
          </Grid>
          <Grid item className={classes.gridItem}>
            <TextField
              label="Nome cientifico"
              fullWidth
              onChange={({ target }) => setScientificName(target.value)}
              helperText="Digite o nome cientifico da planta"
            />
          </Grid>
          <Grid item className={classes.gridItem}>
            <TextField
              label="Descrição"
              fullWidth
              onChange={({ target }) => setDescription(target.value)}
              helperText="Digite uma descrição da planta"
            />
          </Grid>
          {/* <Grid item className={classes.gridItem}>
						<TextField 
							label="Familia"
							fullWidth
							helperText="Digite a familia da planta"
						/>
					</Grid>
					<Grid item className={classes.gridItem}>
						<TextField 
							label="Origem"
							fullWidth
							helperText="Digite a origem da planta"
						/>
					</Grid>
					<Grid item className={classes.gridItem}>
						<TextField 
							label="Luminosidade"
							fullWidth
							helperText="Digite a luminosidade da planta"
						/>
					</Grid>
					<Grid item className={classes.gridItem}>
						<TextField 
							label="Clima"
							fullWidth
							helperText="Digite o clima da planta"
						/>
					</Grid> */}
          <Grid item className={classes.gridItem}>
            <FormControl style={{ width: '100%' }}>
              <InputLabel id="demo-simple-select-label">Categoria</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={idCategory}
                onChange={({ target }) => setIdCategory(target.value)}
              >
                {listCategories.map((category, id) => <MenuItem key={id} value={category.id_category}>{category.name}</MenuItem>)}
              </Select>
            </FormControl>
          </Grid>
          <Grid item className={clsx([classes.gridItem, classes.gridSlider])}>
            <Typography>Humidade do ar</Typography>
            <Slider
              value={humidityAir}
              onChange={(ter, gr) => setHumidityAir(gr)}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              //getAriaValueText={(value) => { console.log(`${value}op`); return `${value}op`}}
              valueLabelFormat={(value) => { return `${value}%` }}
              classes={{
                valueLabel: classes.slider
              }}
            />
          </Grid>
          <Grid item className={clsx([classes.gridItem, classes.gridSlider])}>
            <Typography>Humidade do solo</Typography>
            <Slider
              value={humidityGround}
              onChange={(ter, gr) => setHumidityGround(gr)}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              //getAriaValueText={(value) => { console.log(`${value}op`); return `${value}op`}}
              valueLabelFormat={(value) => { return `${value}%` }}
              classes={{
                valueLabel: classes.slider
              }}
            />
          </Grid>
          <Grid item className={clsx([classes.gridItem, classes.gridSlider])}>
            <Typography>Ciclo de vida</Typography>
            <Slider
              value={cycle}
              onChange={(ter, gr) => setCycle(gr)}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              //getAriaValueText={(value) => { console.log(`${value}op`); return `${value}op` }}
              valueLabelFormat={(value) => { return `${value}d` }}
              classes={{
                valueLabel: classes.slider
              }}
              min={0}
              max={366}
            />
          </Grid>
          <Grid item className={clsx([classes.gridItem, classes.gridSlider])}>
            <Typography>Temperatura</Typography>
            <Slider
              value={temperature}
              onChange={(ter, gr) => setTemperature(gr)}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              //getAriaValueText={(value) => { console.log(`${value}op`); return `${value}op`}}
              valueLabelFormat={(value) => { return `${value} °C` }}
              classes={{
                valueLabel: classes.slider
              }}
            />
          </Grid>
          <Grid item className={clsx([classes.gridItem, classes.gridSlider])}>
            <Typography>pH</Typography>
            <Slider
              value={pH}
              onChange={(ter, gr) => setpH(gr)}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              //getAriaValueText={(value) => { console.log(`${value}op`); return `${value}op`}}
              valueLabelFormat={(value) => { return `${value}%` }}
              classes={{
                valueLabel: classes.slider
              }}
            />
          </Grid>
          <Grid item className={clsx([classes.gridItem, classes.gridSlider])}>
            <Typography>Altura</Typography>
            <Slider
              value={height}
              onChange={(ter, gr) => setHeight(gr)}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              //getAriaValueText={(value) => { console.log(`${value}op`); return `${value}op`}}
              valueLabelFormat={(value) => { return `${value} cm` }}
              classes={{
                valueLabel: classes.slider
              }}
            />
          </Grid>
        </Grid>
      </DialogAdd>
    </Container>
  )
};
