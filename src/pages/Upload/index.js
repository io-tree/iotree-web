import React, { useState, useEffect } from 'react';
import moment from 'moment'
import { useSnackbar } from 'notistack';
import {
  Container,
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  Button
} from '@material-ui/core';

import { useSelector, useDispatch } from 'react-redux'
import { appActions } from '../../redux'
import api from '../../services/api';
import useStyles from './styles'

export default ({ history }) => {

  const listMenu = [
    'Plantas',
    'Categorias'
  ]

  const classes = useStyles();
  const dispatch = useDispatch()
  const isLoading = useSelector(state => state.isLoading)
  const [prop, setProp] = useState()
  const { enqueueSnackbar } = useSnackbar();
  const [listCategory, setListCategory] = useState([]);
  const [listPlant, setListPlant] = useState([])
  const [id, setId] = useState(0)
  const [img, setImg] = useState('')

  useEffect(() => {
    (async () => {
      const listCategoryData = await api.get('/categories')
      const listPlantData = await api.get('/plants')
      setListCategory(listCategoryData.data.body)
      setListPlant(listPlantData.data.body)
    })()
  }, []);

  const onSubmit = () => {
    let form = new FormData();
    form.append('img', img, img.name);
    form.append('path', (prop === 'Plantas' ? 'plants' : 'categories'));
    form.append('id', id);

    api.post('/uploads', form, {
      headers: {
        ...api.defaults.headers,
        'Content-Type': 'multipart/form-data',
      },
    }).then(async res => {
      enqueueSnackbar('Imagem enviada', {
        variant: 'success',
      })
    }).catch(err => {
      enqueueSnackbar('Não foi possivel enviar a imagem', {
        variant: 'error',
      })
    })
  }

  return (
    <Container
      disableGutters
      maxWidth={false}
      style={{
        padding: 15,
        borderRadius: 5,
        height: '100%',
        background: 'white',
      }}
    >
      {!isLoading &&
        <div
          style={{
            display: 'flex',
            flexDirection: 'column'
          }}
        >
          <FormControl className={classes.formControl}>
            <InputLabel id="propId">Propriedade</InputLabel>
            <Select
              labelId="propId"
              style={{
                width: 500
              }}
              value={prop}
              onChange={({ target }) => setProp(target.value)}
            >
              {listMenu.map(item =>
                <MenuItem key={item} value={item}>{item}</MenuItem>
              )}
            </Select>
          </FormControl>
          {prop === 'Plantas' &&
            <FormControl className={classes.formControl}>
              <InputLabel id="propId">Item</InputLabel>
              <Select
                labelId="propId"
                style={{
                  width: 500
                }}
                value={id}
                onChange={({ target }) => setId(target.value)}
              >
                {listPlant.map(item =>
                  <MenuItem key={item.id_plant} value={item.id_plant}>{item.name}</MenuItem>
                )}
              </Select>
            </FormControl>
          }
          {prop === 'Categorias' &&
            <FormControl className={classes.formControl}>
              <InputLabel id="propId">Item</InputLabel>
              <Select
                labelId="propId"
                style={{
                  width: 500
                }}
                value={id}
                onChange={({ target }) => setId(target.value)}
              >
                {listCategory.map(item =>
                  <MenuItem key={item.id_category} value={item.id_category}>{item.name}</MenuItem>
                )}
              </Select>
            </FormControl>
          }
          <input
            accept="image/*"
            className={classes.input}
            id="contained-button-file"
            type="file"
            style={{
              display: 'none'
            }}
            onChange={(ev) => setImg(ev.target.files[0])}
          />
          <label htmlFor="contained-button-file">
            <Button variant="contained" color="primary" component="span">
              Upload
            </Button>
          </label>
          <Button
            variant="contained"
            color="primary"
            style={{
              margin: '20px 0',
              color: 'white'
            }}
            onClick={onSubmit}
          >
            Enviar
          </Button>
        </div>
      }
    </Container>
  )
};
