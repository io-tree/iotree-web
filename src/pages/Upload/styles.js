import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  card: {
    width: 220,
    boxShadow: '5px 5px 5px rgba(0,0,0,0.5)'
  },
  formControl: {
    marginBottom: 20
  }
}));