import React from 'react'

export default {
  setLoadingDashboard (isloading) {
    return { type: 'SET_LOADING_DASHBOARD', payload: isloading }
  },
  setUser (user) {
    return { type: 'SET_USER', payload: user }
  }
}