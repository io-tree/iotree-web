import actions from './actions'
import reducers from './reducers'

export const appActions = actions
export const appReducers = reducers