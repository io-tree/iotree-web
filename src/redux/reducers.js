const initialState = {
  user: undefined,
  isLoadingDashboard: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_USER':
      return { ...state, user: action.payload }
    case 'SET_LOADING_DASHBOARD':
      return { ...state, isLoadingDashboard: action.payload }
    default:
      return state
  }
}